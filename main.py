import datetime
from flask import Flask
import json
import pymongo

sys_dict = [{"name": "masoa", "displayName": "משואה"},
            {"name": "even-yesod", "displayName": "אבן יסוד"},
            {"name": "kadur-bdolach", "displayName": "כדור בדולח"}]

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["mydatabase"]
masoaCollection = mydb["masoaCollection"]
evenYesodCollection = mydb["evenYesodCollection"]
kadurBdolachCollection = mydb["kadurBdolachCollection"]


def main():
    app = Flask(__name__)

    @app.route("/systems/")
    def choose_sys():
        return json.dumps(sys_dict)

    @app.route("/systems/<name>/")
    def sys_homepage(name):
        return json.dumps(["features", "requests"])

    @app.route("/systems/<name>/<option>/")
    def sys_options(name, option):
        if name == "masoa":
            if option == "features":
                print("hi")
                return list(masoaCollection.masoaFeaturesDict.find())
            elif option == "requests":
                return masoaCollection.masoaRequestsDict.find()
        elif name == "even-yesod":
            if option == "features":
                return evenYesodCollection.evenYesodFeaturesDict.find()
            elif option == "requests":
                return evenYesodCollection.evenYesodRequestsDict.find()
        elif name == "kadur-bdolach":
            if option == "features":
                return kadurBdolachCollection.kadurBdolachFeaturesDict.find()
            elif option == "requests":
                return kadurBdolachCollection.kadurBdolachRequestsDict.find()

    @app.route("/systems/<name>/contact/")
    def contact(name, form):
        form = {"title": ["content", "type"]}  # TODO get a real form
        username = "tom"  # TODO get a real username
        time_format = "%d/%m/%y %H:%M"
        today = datetime.datetime.today()
        s = today.strftime(time_format)
        d = datetime.datetime.strptime(s, time_format)
        time = d.strftime(time_format)

        for val in form.values():
            new_val = val.append(time)
            new_val = val.append(username)
            print(form.values())

        form = json.dumps(form)

        if name == "masoa":
            masoaCollection.masoaRequestsDict.insert_one(form)
        elif name == "even-yesod":
            evenYesodCollection.evenYesodRequestsDict.insert_one(form)
        elif name == "kadur-bdolach":
            kadurBdolachCollection.kadurBdolachRequestsDict.insert_one(form)
        return "yayyyyyy we have saved your request"

    app.run(host="0.0.0.0", debug=True)


if __name__ == "__main__":
    main()
